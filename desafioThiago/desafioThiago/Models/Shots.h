//
//  Shots.h
//  desafioThiago
//
//  Created by Thiago Alves on 03/04/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTLModel.h"
#import "MTLJSONAdapter.h" // defines MTLJSONSerializing

@interface Shots : MTLModel <MTLJSONSerializing>

//@property (nonatomic) NSInteger *idShot;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *descriptionText;
@property (nonatomic) NSString *published;
@property (nonatomic) NSDictionary *image;

@end

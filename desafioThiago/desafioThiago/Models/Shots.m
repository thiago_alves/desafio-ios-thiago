//
//  Shots.m
//  desafioThiago
//
//  Created by Thiago Alves on 03/04/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

#import "Shots.h"

@implementation Shots

+ (NSDictionary *)JSONKeyPathsByPropertyKey {

    return @{
             @"title" : @"title",
             @"descriptionText": @"description",
             @"published":@"updated_at",
             @"image":@"images"
             };
}

//JSON: (
//       {
//           animated = 0;
//           attachments =         (
//           );
//           description = "<p>App do Dia na App Store</p>";
//           height = 300;
//           "html_url" = "https://dribbble.com/shots/4481858-Appito";
//           id = 4481858;

//           images =         {
//               hidpi = "<null>";
//               normal = "https://cdn.dribbble.com/users/2242653/screenshots/4481858/img_0964.jpg";
//               teaser = "https://cdn.dribbble.com/users/2242653/screenshots/4481858/img_0964_teaser.jpg";
//           };

@end

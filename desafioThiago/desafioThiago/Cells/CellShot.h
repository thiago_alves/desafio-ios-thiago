//
//  CellShot.h
//  desafioThiago
//
//  Created by Thiago Alves on 02/04/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Shots.h"

@interface CellShot : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UIImageView *imgShot;

-(void)configWith:(Shots *)shot;

@end

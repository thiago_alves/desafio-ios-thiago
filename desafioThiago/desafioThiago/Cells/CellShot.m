//
//  CellShot.m
//  desafioThiago
//
//  Created by Thiago Alves on 02/04/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

#import "CellShot.h"
#import "Shots.h"

@implementation CellShot

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)configWith:(Shots *)shot {
    [_lblTitle setText: [shot title]];
    [_lblDate setText: [shot published]];

    NSString *imagePath = shot.image[@"normal"];
    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: imagePath]];

    _imgShot.image = [UIImage imageWithData:imageData];

}

@end

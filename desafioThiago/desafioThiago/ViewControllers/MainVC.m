//
//  MainVC.m
//  desafioThiago
//
//  Created by Thiago Alves on 02/04/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

#import "MainVC.h"
#import "AFNetworking.h"

@interface MainVC () {
    NSMutableArray *list;

    UIRefreshControl *refreshControl;
}

@end

@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [_tableView registerNib:[UINib nibWithNibName:@"CellShot" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CellShot"];

    _tableView.estimatedRowHeight = 193;
    _tableView.rowHeight = UITableViewAutomaticDimension;

    refreshControl = [[UIRefreshControl alloc]init];
    [_tableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(loadAll) forControlEvents:UIControlEventValueChanged];

    [self loadAll];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadAll {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

    [manager GET: @"https://api.dribbble.com/v2/user/shots?access_token=c4b70e9a91d027aeb588ab9d2c88c491fdd7be02caa6c7aee1a344eab8a97ca7" parameters: nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {

        [refreshControl endRefreshing];

        NSLog(@"JSON: %@", responseObject);

        NSError *error;
        NSArray *appInfos = [MTLJSONAdapter modelsOfClass:[Shots class] fromJSONArray:responseObject error:&error];
        if (error) {
            NSLog(@"Couldn't convert app infos JSON to ChoosyAppInfo models: %@", error);
        } else {
            list = [[NSMutableArray alloc]initWithArray:appInfos];
            [_tableView reloadData];
            NSLog(@"JSON: %@", appInfos);
        }

    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [refreshControl endRefreshing];
    }];
}

#pragma mark - UITableViewDataSource UITableViewDelegate

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {

    CellShot *cell = [tableView dequeueReusableCellWithIdentifier:@"CellShot"];
    [cell configWith: [list objectAtIndex:indexPath.row]];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [list count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    DetailVC * vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];

    vc.shot = [list objectAtIndex:indexPath.row];

    [[self navigationController]pushViewController: vc animated:YES];
}

@end

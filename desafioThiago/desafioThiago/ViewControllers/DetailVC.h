//
//  DetailVC.h
//  desafioThiago
//
//  Created by Thiago Alves on 03/04/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Shots.h"

@interface DetailVC : UIViewController

@property (nonatomic) Shots *shot;


@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;
@property (weak, nonatomic) IBOutlet UIImageView *imgDetail;

@end

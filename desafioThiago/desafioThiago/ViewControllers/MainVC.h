//
//  MainVC.h
//  desafioThiago
//
//  Created by Thiago Alves on 02/04/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "Shots.h"
#import "CellShot.h"
#import "DetailVC.h"

@interface MainVC : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end

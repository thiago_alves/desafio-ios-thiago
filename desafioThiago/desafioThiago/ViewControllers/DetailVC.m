//
//  DetailVC.m
//  desafioThiago
//
//  Created by Thiago Alves on 03/04/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

#import "DetailVC.h"

@interface DetailVC ()

@end

@implementation DetailVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [_lblTitle setText:[_shot title]];
    
    NSString *htmlString = _shot.descriptionText;
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    _txtDescription.attributedText = attributedString;

    NSString *imagePath = _shot.image[@"normal"];
    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: imagePath]];

    _imgDetail.image = [UIImage imageWithData:imageData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

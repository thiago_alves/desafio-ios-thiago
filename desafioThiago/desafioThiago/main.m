//
//  main.m
//  desafioThiago
//
//  Created by Thiago Alves on 02/04/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
